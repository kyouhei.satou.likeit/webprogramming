

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServ
 */
@WebServlet("/UpdateServ")
public class UpdateServ extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		System.out.println(id);
		UserDao userDao = new UserDao();
		User user = userDao.getPrimaryId(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher =
				request.getRequestDispatcher("/WEB-INF/jsp/userInfUpdate.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		String loginId = request.getParameter("loginId");

		if(userName.equals("") || birthDate.equals("")) {
			 request.setAttribute("errMsg", "入力された情報は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userInfUpdate.jsp");
				dispatcher.forward(request, response);
				return;
		}
		else if(!(password.equals(checkPassword))){
			 request.setAttribute("errMsg", "入力された情報は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userInfUpdate.jsp");
				dispatcher.forward(request, response);
				return;
		}
		UserDao userDao = new UserDao();
		userDao.updateUser(password, userName, birthDate, loginId);

		response.sendRedirect("UserListServ");



	}

}
