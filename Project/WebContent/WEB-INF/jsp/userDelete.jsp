<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー削除確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

     <link href="style.css" rel="stylesheet" type="text/css" />

</head>
	<body>
	<form action="UserDeleteServ" method="post">
	<input type="hidden" name="loginId" value="${user.loginId}">
      <div class="row navbar-dark bg-dark">
             <div class= "col-sm-5">
             </div>
             <div class= "col-sm-5 navbar-brand">
             <li class="navbar-text">${userInfo.name}さん</li>
             </div>
             <div class= "col-sm-0">
                    <a href="LogoutServ" class="navbar-link logout-link">ログアウト</a>
             </div>
         </div>

        <div class= "row mt-5">
             <link href="style.css" rel="stylesheet" type="text/css" />
            <link href="userList.css" rel="stylesheet" type="text/css" />
            <div class= "col-sm-4">
            </div>
            <h1>ユーザー削除確認</h1>
        </div>

        <div class= "row mt-5">
            <div class= "col-sm-4">
            </div>
            ログインID：${user.loginId}
        </div>

        <div class= row>
            <div class= "col-sm-4">
            </div>
            を本当に削除してよろしいでしょうか。
            </div>

        <div class= "row mt-5">
        <div class= "col-sm-4">
            </div>
        <div class= "col-sm-3">
            <button type="button" onclick= "history.back()">キャンセル</button>
            </div>
         <input type="submit" value="OK">
        </div>

    　　
	</form>
    </body>
</html>