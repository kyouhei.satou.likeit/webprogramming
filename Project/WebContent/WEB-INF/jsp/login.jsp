<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

     <link href="style.css" rel="stylesheet" type="text/css" />

</head>
	<body>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>

	</c:if>
	<form action="LoginServ" method="post">
           <div class="row mt-5">
    <div class="col-sm-4">
    </div>

        <h1>ログイン画面</h1>
     </div>

        <div class="row mt-5">
           <div class="col-sm-3">
           </div>
           <div class="col-sm-2">
            ログインID
            </div>
           <div class="col-sm-4">
            <input type="text" name= "loginId" style="width:200px;" class="form-control">
            <div class="col-sm-4">
            </div>
           </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-2">
            パスワード
            </div>
           <div class="col-sm-4">
            <input type="text" name= "password" style="width:200px;" class="form-control">
            <div class="col-sm-4">
            </div>
            </div>
        </div>


            <div class="col-sm-2 mx-auto mt-5">
           <input type="submit" value="ログイン">
        </div>



	</form>
	</body>
</html>
