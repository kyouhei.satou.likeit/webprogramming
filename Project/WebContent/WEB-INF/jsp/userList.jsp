<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="userList.css" rel="stylesheet" type="text/css" />

</head>
<body>

		<div class="row navbar-dark bg-dark">
			<div class="col-sm-5"></div>
			<div class="col-sm-5 navbar-brand">
				<li class="navbar-text">${userInfo.name}さん</li>
			</div>
			<div class="col-sm-0">
				<a href="LogoutServ" class="navbar-link logout-link">ログアウト</a>
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<h1>ユーザー一覧</h1>
		</div>

		<div class="row mt-5">
			<div class="col-sm-10"></div>

		<a class="btn btn-primary" href="RegisterServ" role="button">新規登録</a>

		</div>
	<form action="UserListServ" method="post">
		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-1">
				<j1>ログインID</j1>
			</div>
			<div class="col-sm-">
				<input type="text" name="loginId" style="width: 465px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-1">
				<j1> ユーザー名</j1>
			</div>
			<div class="col-sm-">
				<input type="text" name="userName" style="width: 465px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-1">
				<j1> 生年月日</j1>
			</div>
			<div class="col-sm-0">
				<input type="date" name="birthStart" style="width: 150px;">
			</div>
			<div class="col-sm-1">～</div>
			<div class="col-sm-0">
				<input type="date" name="birthEnd" style="width: 150px;">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-10"></div>
			<input type="submit" value="検索">

		</div>

		<table class="table table-bordered mt-5">
			<thead>

				<tr class="table-active">

					<th scope="col">ログインID</th>
					<th scope="col">ユーザー名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>

				</tr>


			</thead>


			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>

						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>

						<!-- TODO 未実装；ログインボタンの表示制御を行う -->

						<td>
						<c:if test="${userInfo.loginId == 'admin'}" >
						<a class="btn btn-primary" href="UserDetailServ?id=${user.id}">参照</a>


						<a class="btn btn-success" href="UpdateServ?id=${user.id}">更新</a>


						<a class="btn btn-danger" href="UserDeleteServ?id=${user.id}">削除</a>

					</c:if>

					<c:if test="${userInfo.loginId != 'admin'}" >
						<a class="btn btn-primary" href="UserDetailServ?id=${user.id}">参照</a>

						<c:if test="${userInfo.loginId == user.loginId}" >
						<a class="btn btn-success" href="UpdateServ?id=${user.id}">更新</a>
						</c:if>



					</c:if>

					</td>

					</tr>

				</c:forEach>
			</tbody>

		</table>

	</form>

</body>
</html>
